# -*- coding: utf-8 -*-
__author__ = 'Liu'


class schoolMember:
    '''This means every school member'''

    def __init__(self, name, age):
        self.name = name
        self.age = age
        print("Initialized SchoolMember:%s" % self.name)

    def tell(self):
        '''Details of School Member'''
        print("Name:%s,Age:%s" % (self.name, self.age))


class teacher(schoolMember):
    '''As a teacher'''

    def __init__(self, name, age, salary):
        # 继承了原来的方法就得把最开始的类所需要的self，name,age给带上
        # Python不会自动调用基本类的构造函数，得你自己来，并且填好参数
        schoolMember.__init__(self, name, age)
        self.salary = salary
        print("Initialized teacher: %s" % self.name)

    def tell(self):
        schoolMember.tell(self)
        print("Salary :%d" % self.salary)

    def function():
        pass

class students(schoolMember):
    '''Initialized as a student'''

    def __init__(self, name, age, marks):
        schoolMember.__init__(self, name, age)
        self.marks = marks
        print('Initialized as a student:%s' % self.name)

    def tell(self):
        # 引用的也要加self
        schoolMember.tell(self)
        print('The student mark is %s' % self.marks)


t = teacher('Matt', 28, 28000)
s = students('Liu', 18, 95)

members = [t, s]
for member in members:
    member.tell()