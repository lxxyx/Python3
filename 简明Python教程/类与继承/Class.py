# -*- coding: utf-8 -*-
__author__ = 'Liu'


class person:
    ''''This is a class'''
    population = 0
    # 等同于java的构造函数。可以通过传参数来完成某些操作。
    def __init__(self, name):
        self.name = name
        print("This name is %s" % self.name)
        person.population += 1

    # 但对象不再使用的时候，_del_方法会自动运行
    def __del__(self):
        print("%s says bye." % self.name)
        person.population -= 1
        if person.population == 0:
            print("I am the last one")
        else:
            print('There are still %d people left' % person.population)

    def sayHi(self):
        print("Hi,my name is ", self.name)

    def howmany(self):
        if person.population == 1:
            print("There is only one")
        else:
            print('There are still %d people left' % person.population)


Swaroop = person('Swaroop')
# Swaroop.sayHi()
# Swaroop.howmany()

kalam = person('Abl kalam')
# kalam.sayHi()
# kalam.howmany()

# Swaroop.sayHi()
# Swaroop.howmany()
