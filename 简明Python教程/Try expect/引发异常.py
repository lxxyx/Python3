__author__ = 'Liu'
# 由于python3 和2的差异，难以进行
class ShortInputException(Exception):
    '''Short question'''

    def __init__(self, length, atleast):
        Exception.__init__(self)
        self.length = length
        self.atleast = atleast


try:
    s = input("Input >>>")
    if len(s) < 3:
        raise ShortInputException(len(s), 3)
except EOFError:
    print("EOF")
else:
    print("Nothing")