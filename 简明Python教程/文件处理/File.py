# -*- coding: utf-8 -*-
__author__ = 'Liu'

poem = '''
123
456
123
'''

f = open("poem.txt", 'w')
f.write(poem)
f.close()
# Python3 移除了file函数改用open()
# open指定的文件名要用双引号标明，或者直接用一个变量代替直接输入的文件名
f = open("poem.txt", 'r')

while True:
    line = f.readline()
    if len(line) == 0:
        break
    print(line)

f.close()