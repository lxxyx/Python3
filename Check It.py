# -*- coding: utf-8 -*-
__author__ = 'Liu'

# 导入requests模块，事实上证明这个模块挺简单
import requests

# 直接定义url并和username一起传入网站
url = 'http://222.204.3.210/ssdf/Account/LogOn?ReturnUrl=%2fssdf%2fEEMQuery%2fEEMBalance'
username = {'UserName': '090230'}
# 意思是得到post之后的网站.post的是username的数据
r = requests.post(url, data=username)
# 设定编码格式,发现除了utf-8,其他的都不好使
r.encoding = 'utf-8'
print(r.text)


def fox(x):
    x = 2 * x
    return x


x = 1
x = fox(x + 1) + fox(x + 2)
print(x)
